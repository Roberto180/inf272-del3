﻿using INF272_del3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using INF272_del3.Viewmodels;

namespace INF272_del3.Controllers
{
    public class AdminReportController : Controller
    {
        // GET: AdminReport
        private LinkDatabaseTDOEntities4 db = new LinkDatabaseTDOEntities4();
        public ActionResult Index()
        {
            ViewBag.BusinessCategoryID = new SelectList(db.Business_Category, "BusinessCategoryID", "Business_Categoty");
            ViewBag.ProvinceID = new SelectList(db.Provinces, "ProvinceID", "ProvinceName");

            var sum1 = db.Users.Count();
            var sum2 = db.Users.Where(zz => zz.AccountTypeID == 2).Count();
            var sum3 = db.Users.Where(zz => zz.AccountTypeID == 1).Count();
            ViewBag.user = sum1;
            ViewBag.Freelancer = sum2;
            ViewBag.Employer = sum3;
            return View();
        }


        public JsonResult table(int ProvinceID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<chart> vm = new List<chart>();
            List<Employer> emp = db.Employers.Include("Subhurb").Include(zz => zz.Subhurb.City).Where(zz => zz.Subhurb.City.ProvinceID == ProvinceID).ToList();
            //List<Freelancer> free = db.Freelancers.Include("Subhurb").Include(zz=>zz.Subhurb.City).Include(zz=>zz.Business_Category).Where(zz => zz.BusinessCategoryID == BusinessCategoryID).ToList();

            foreach (Employer x in emp)
            {
                chart table = new chart();
                table.Name = x.Name;
                table.Surname = x.Surname;
                table.CityName = x.Subhurb.City.CityName;
                table.SubhurbName = x.Subhurb.SubhurbName;
                vm.Add(table);
            }

            return new JsonResult { Data = vm, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }


        public JsonResult ChartData(int BusinessCategoryID)
        {
            db.Configuration.ProxyCreationEnabled = false;

           
            var cities = db.Freelancers.Include(zz => zz.Subhurb.City).Where(zz => zz.BusinessCategoryID == BusinessCategoryID).GroupBy(zz => zz.Subhurb.City.CityName).ToList();

           
            List<report> chartdata = new List<report>();
            foreach (var group in cities)
            {
                report vm = new report();
                vm.CityName = group.Key;
                vm.Total = group.Count();
                chartdata.Add(vm);
            }

            return new JsonResult { Data = chartdata, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}
    
