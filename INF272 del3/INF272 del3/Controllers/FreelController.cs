﻿using INF272_del3.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace INF272_del3.Controllers
{
    public class FreelController : Controller
    {
        private LinkDatabaseTDOEntities4 db = new LinkDatabaseTDOEntities4();

        
        public ActionResult freeProfile(int id)
        {
            Freelancer freelancer = db.Freelancers.Where(zz => zz.UserID == id).FirstOrDefault();
            ViewBag.fl = freelancer;
            ViewBag.city = freelancer.Subhurb.City.CityName;
            User user = db.Users.Where(zz => zz.UserID == id).Include(zz => zz.Image).FirstOrDefault();
            ViewBag.user = user;
            List<PortfolioImage> pf = db.PortfolioImages.Where(zz => zz.FreelancerID == freelancer.FreelancerID).Include(zz => zz.Image).Include(zz => zz.Freelancer).ToList();
            return View(pf);

            
        }
        public ActionResult Profiles(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Freelancer freelancer = db.Freelancers.Find(id);
            if (freelancer == null)
            {
                return HttpNotFound();
            }
            return View(freelancer);
        }

        public ActionResult Editprofile(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Freelancer freelancer = db.Freelancers.Where(zz => zz.UserID == id).FirstOrDefault();
            if (freelancer == null)
            {
                return HttpNotFound();
            }

            return View(freelancer);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editprofile(Freelancer freelancer, int id)
        {
            Int32 freelancerid = id;
            var StudentData = db.Freelancers.Where(x => x.UserID == freelancerid).FirstOrDefault();
            if (ModelState.IsValid)
            {
                StudentData.PortfolioConcept = freelancer.PortfolioConcept;
                db.Entry(StudentData).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("freeProfile", "Freel", new { id = freelancerid });
            }

            return RedirectToAction("freeProfile", "Freel");
        }

        public ActionResult Clients(int id)
        {
            List<EMP_Freelancer> clients = db.EMP_Freelancer.Where(zz => zz.FreelancerID ==id).Include(zz => zz.Employer).Include(zz => zz.Freelancer.Subhurb).ToList();
            var free = db.Freelancers.Where(zz => zz.FreelancerID == id).Select(zz => zz.UserID).FirstOrDefault();
            ViewBag.user = free;
            return View(clients);


        }
    }
}
