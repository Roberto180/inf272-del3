﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using INF272_del3.Models;

namespace INF272_del3.Controllers
{
    public class EmployerController : Controller
    {
        private LinkDatabaseTDOEntities4 db = new LinkDatabaseTDOEntities4();

        // GET: Employer
        public ActionResult Index( int id)
        {   Employer emp = db.Employers.Where(zz => zz.UserID == id).FirstOrDefault();
            ViewBag.emp = emp;
            User user = db.Users.Where(zz => zz.UserID == id).Include(zz => zz.Image).FirstOrDefault();
            ViewBag.user = user;
            TempData["eID"] = emp.EmployerID;
            TempData["uID"] = id;
            var businesscat = db.Business_Category.Include(b => b.Freelancers);
            return View(businesscat.ToList());
        }


        public ActionResult PotentialFreelancer(int id, int? h, int? zzz)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<Freelancer> free = db.Freelancers.Include(zz => zz.Subhurb).Include(zz => zz.Subhurb.City).Include(zz => zz.Subhurb.City.Province).Include(zz=>zz.User.Image).Where(zz => zz.BusinessCategoryID == id).ToList();
            var title = db.Business_Category.Where(zz => zz.BusinessCategoryID == id).Select(zz => zz.Business_Categoty).FirstOrDefault();
            var em = db.Employers.Include(zz => zz.User).Where(zz => zz.EmployerID == h).Select(zz => zz.UserID).FirstOrDefault();
            
          if(zzz == null)
            {
                ViewBag.zzz = em;
            }
            else
            {
                ViewBag.zzz = zzz;
            }
            

           
          
            ViewBag.title = title;
            TempData["zID"] = h;
            
             
            return View(free);
        }

        public ActionResult freeProfile(int? id)
        {
            Freelancer freelancer = db.Freelancers.Where(zz => zz.UserID == id).FirstOrDefault();
            ViewBag.fl = freelancer;
            ViewBag.city = freelancer.Subhurb.City.CityName;
            User user = db.Users.Where(zz => zz.UserID == id).Include(zz => zz.Image).FirstOrDefault();
            ViewBag.user = user;


            if (TempData["eID"] != null)
            {
                ViewBag.eID = (int)TempData["eID"];
            }
            else
            {
                ViewBag.eID = (int)TempData["zID"];
            }
            
         
            List<PortfolioImage> pf = db.PortfolioImages.Where(zz => zz.FreelancerID == freelancer.FreelancerID).Include(zz => zz.Image).Include(zz => zz.Freelancer).ToList();
            return View(pf);


        }


        public ActionResult PastBookings(int id)
        {
            List<EMP_Freelancer> book = db.EMP_Freelancer.Where(zz => zz.EmployerID == id).Include(zz => zz.Freelancer).Include(zz=>zz.Freelancer.Subhurb).ToList();
            var emp = book.Select(zz => zz.Employer.User.UserID).FirstOrDefault();
            ViewBag.user = emp;
            return View(book);
        }




    }
}
