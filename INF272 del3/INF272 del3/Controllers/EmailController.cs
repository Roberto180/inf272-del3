﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using INF272_del3.Models;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace INF272_del3.Controllers
{
    public class EmailController : Controller
    {
        // GET: Email
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Contact(EmailFormModel model)
        {
            if (ModelState.IsValid)
            {
                var body = "<p>Email From: {0} ({1})</p><p>Message:</p><p>{2}</p>";
                var message = new MailMessage();
                message.To.Add(new MailAddress("vrajeyyphotography@gmail.com"));  // replace with valid value 
                message.From = new MailAddress("swamibapa4vraj@gmail.com");  // replace with valid value
                message.Subject = "Test";
                message.Body = string.Format(body, model.FromName, model.FromEmail, model.Message);
                message.IsBodyHtml = true;

                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "swamibapa4vraj@gmail.com",  // replace with valid value
                        Password = "VrajAd1t1"  // replace with valid value
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    await smtp.SendMailAsync(message);
                    return RedirectToAction("Sent");
                }
            }
            return View(model);
        }

        public ActionResult Sent()
        {
            return View();
        }


    }
}