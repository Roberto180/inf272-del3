﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using INF272_del3.Models;

namespace INF272_del3.Controllers
{
    public class SubhurbsController : Controller
    {
        private LinkDatabaseTDOEntities db = new LinkDatabaseTDOEntities();

        // GET: Subhurbs
        public ActionResult Index()
        {
            var subhurbs = db.Subhurbs.Include(s => s.City);
            return View(subhurbs.ToList());
        }

        // GET: Subhurbs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subhurb subhurb = db.Subhurbs.Find(id);
            if (subhurb == null)
            {
                return HttpNotFound();
            }
            return View(subhurb);
        }

        // GET: Subhurbs/Create
        public ActionResult Create()
        {
            ViewBag.CityID = new SelectList(db.Cities, "CityID", "CityName");
            return View();
        }

        // POST: Subhurbs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SubhurbID,SubhurbName,CityID")] Subhurb subhurb)
        {
            if (ModelState.IsValid)
            {
                db.Subhurbs.Add(subhurb);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CityID = new SelectList(db.Cities, "CityID", "CityName", subhurb.CityID);
            return View(subhurb);
        }

        // GET: Subhurbs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subhurb subhurb = db.Subhurbs.Find(id);
            if (subhurb == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityID = new SelectList(db.Cities, "CityID", "CityName", subhurb.CityID);
            return View(subhurb);
        }

        // POST: Subhurbs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SubhurbID,SubhurbName,CityID")] Subhurb subhurb)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subhurb).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CityID = new SelectList(db.Cities, "CityID", "CityName", subhurb.CityID);
            return View(subhurb);
        }

        // GET: Subhurbs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subhurb subhurb = db.Subhurbs.Find(id);
            if (subhurb == null)
            {
                return HttpNotFound();
            }
            return View(subhurb);
        }

        // POST: Subhurbs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Subhurb subhurb = db.Subhurbs.Find(id);
            db.Subhurbs.Remove(subhurb);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
