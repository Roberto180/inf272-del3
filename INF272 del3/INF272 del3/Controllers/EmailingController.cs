﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using INF272_del3.Models;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Data.Entity;

namespace INF272_del3.Controllers
{
    public class EmailingController : Controller
    {

        private LinkDatabaseTDOEntities4 db = new LinkDatabaseTDOEntities4();
        // GET: Emailing
        public ActionResult Contact()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Contact(EmailFormModel model, int id, int ide)
        {
            //if (ModelState.IsValid)
            //{

            var emp = db.Employers.Where(zz => zz.EmployerID == id).Include(xx => xx.User).FirstOrDefault();
            var free = db.Freelancers.Where(zz => zz.FreelancerID == id).Include(xx => xx.User).FirstOrDefault();
                var femail = free.User.Email;
            var eemail = emp.User.Email;
            var x = emp.User.UserID;
                var body = "<p> Hi there, you have a message from:" + femail + "{0} ({1}) </p> <p>Message:</p> <p> Their email is:" + eemail + ", please do contact them should you be intrested </p> {2} ";
                var message = new MailMessage();
                message.To.Add(new MailAddress(femail));  
                message.From = new MailAddress("donotreplylink@gmail.com");  
                message.Subject = "LINK: Urgent Enquiry";
                message.Body = string.Format(body, model.FromName, model.FromEmail, model.Message);
                message.IsBodyHtml = true; 

                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "donotreplylink@gmail.com",  // do not share!
                        Password = "12345Link"  // please treat this with respect
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    await smtp.SendMailAsync(message);
                    return RedirectToAction("Index","Employer", new { id = x});
                }
            //}
            return View(model);
        }

        public ActionResult Sent()
        {
            return View();
        }
    }


}