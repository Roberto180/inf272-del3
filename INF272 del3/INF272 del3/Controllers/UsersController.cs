﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using INF272_del3.Models;

namespace INF272_del3.Controllers
{
    public class UsersController : Controller
    {
        private LinkDatabaseTDOEntities4 db = new LinkDatabaseTDOEntities4();

        // GET: Users
        //public ActionResult Index()
        //{
        //    db.Configuration.ProxyCreationEnabled = false;
        //    var users = db.Users.Include(u => u.Account_Type).Include(u => u.Image).Include(u => u.Security_Questions);
        //    return View(users.ToList());
        //}


        public ActionResult Index(string sortOrder, string searchString)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.EmailSortParm = sortOrder == "Email" ? "Email_desc" : "Email";
            var users = from s in db.Users
                           select s;

            if (!String.IsNullOrEmpty(searchString))
            {
                users = users.Where(s => s.UserName.Contains(searchString)
                                       || s.Email.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "name_desc":
                    users = users.OrderByDescending(s => s.UserName);
                    break;
                case "Email":
                    users = users.OrderBy(s => s.Email);
                    break;
                case "Email_desc":
                    users = users.OrderBy(s => s.Email);
                    break;
                default:
                    users = users.OrderBy(s => s.UserName);
                    break;
            }
            return View(users.ToList());
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            ViewBag.AccountTypeID = new SelectList(db.Account_Type, "AccountTypeID", "AccDescription");
            ViewBag.ImageID = new SelectList(db.Images, "ImageID", "ImgSource");
            ViewBag.SecurityID = new SelectList(db.Security_Questions, "SecurityID", "Security_QuestionPhrase");
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserID,Userpassword,UserName,Email,UserStatus,ImageID,AccountTypeID,SecurityID")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AccountTypeID = new SelectList(db.Account_Type, "AccountTypeID", "AccDescription", user.AccountTypeID);
            ViewBag.ImageID = new SelectList(db.Images, "ImageID", "ImgSource", user.ImageID);
            ViewBag.SecurityID = new SelectList(db.Security_Questions, "SecurityID", "Security_QuestionPhrase", user.SecurityID);
            return View(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.AccountTypeID = new SelectList(db.Account_Type, "AccountTypeID", "AccDescription", user.AccountTypeID);
            ViewBag.ImageID = new SelectList(db.Images, "ImageID", "ImgSource", user.ImageID);
            ViewBag.SecurityID = new SelectList(db.Security_Questions, "SecurityID", "Security_QuestionPhrase", user.SecurityID);
            return View(user);
        }

        // POST: Users/Edit/5
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserID,Userpassword,UserName,Email,UserStatus,ImageID,AccountTypeID,SecurityID")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AccountTypeID = new SelectList(db.Account_Type, "AccountTypeID", "AccDescription", user.AccountTypeID);
            ViewBag.ImageID = new SelectList(db.Images, "ImageID", "ImgSource", user.ImageID);
            ViewBag.SecurityID = new SelectList(db.Security_Questions, "SecurityID", "Security_QuestionPhrase", user.SecurityID);
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult IndexR()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var users = db.Users.Include(u => u.Account_Type).Include(u => u.Image).Include(u => u.Security_Questions);
            return View(users.ToList());
        }

    }
}
