﻿using INF272_del3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using INF272_del3.Viewmodels;
using System.IO;
using System.Net;
using System.Data.Entity;

namespace INF272_del3.Controllers
{
    public class RegistrationController : Controller
    {
        private LinkDatabaseTDOEntities4 db = new LinkDatabaseTDOEntities4();
        // GET: Registration
        public ActionResult Create()
        {
            ViewBag.AccountTypeID = new SelectList(db.Account_Type, "AccountTypeID", "AccDescription");
            ViewBag.ImageID = new SelectList(db.Images, "ImageID", "ImgSource");
            ViewBag.SecurityID = new SelectList(db.Security_Questions, "SecurityID", "Security_QuestionPhrase");
            return View();
        }
        //awe

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserID,Userpassword,ConfirmPassword,UserName,Email,UserStatus,ImageID,AccountTypeID,SecurityID")] User user, HttpPostedFileBase file)
        {
            string path = Path.Combine(Server.MapPath("~/Images"), Path.GetFileName(file.FileName));
            file.SaveAs(path);
            Image img = new Image();
            img.ImgSource = "~/Images/" + file.FileName;
            db.Images.Add(img);
            db.SaveChanges();
            int imageID = img.ImageID;
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                user.ImageID = imageID;
                user.UserStatus = true;
                var un = user.UserName;
                db.SaveChanges();
                user = new User();
                user = db.Users.Where(zz => zz.UserName == un).FirstOrDefault();
                TempData["uID"] = user.UserID;
                if (user.AccountTypeID == 1)
                { return RedirectToAction("CreateEmployer"); }
                else { return RedirectToAction("CreateFreelancer"); }

            }

            ViewBag.AccountTypeID = new SelectList(db.Account_Type, "AccountTypeID", "AccDescription", user.AccountTypeID);
            ViewBag.ImageID = new SelectList(db.Images, "ImageID", "ImgSource", user.ImageID);
            ViewBag.SecurityID = new SelectList(db.Security_Questions, "SecurityID", "Security_QuestionPhrase", user.SecurityID);
            return View(user);
        }



        public ActionResult CreateFreelancer()
        {
            ViewBag.BusinessCategoryID = new SelectList(db.Business_Category, "BusinessCategoryID", "Business_Categoty");
            ViewBag.SubhurbID = new SelectList(db.Subhurbs, "SubhurbID", "SubhurbName");
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Userpassword");
            ViewBag.CityID = new SelectList(db.Cities, "CityID", "CityName");
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateFreelancer([Bind(Include = " FreelancerID, UserID,Name,Surname,BusinessCategoryID,SubhurbName,CityID")] typeFreelancer freelancer)
        {
            if (ModelState.IsValid)
            {
                Subhurb subz = new Subhurb();
                subz.SubhurbID = 0;
                subz.SubhurbName = freelancer.SubhurbName;
                subz.CityID = freelancer.CityID;
                db.Subhurbs.Add(subz);
                db.SaveChanges();
                //model.orderline.Product = db.Products.Where(zz => zz.ProductID == model.orderline.ProductID).FirstOrDefault();
                //subz = new Subhurb();
                //subz = db.Subhurbs.Where(zz => zz.SubhurbName == freelancer.SubhurbName).FirstOrDefault();
                Freelancer free = new Freelancer();
                free.FreelancerID = freelancer.FreelancerID;
                free.SubhurbID = subz.SubhurbID;
                free.Name = freelancer.Name;
                free.Surname = freelancer.Surname;
                free.BusinessCategoryID = freelancer.BusinessCategoryID;
                free.UserID = (int)TempData["uID"];
                db.Freelancers.Add(free);
                //db.Users.Add(employer);
                db.SaveChanges();

            }
            ViewBag.SubhurbID = new SelectList(db.Subhurbs, "SubhurbID", "SubhurbName");
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Userpassword");
            ViewBag.CityID = new SelectList(db.Cities, "CityID", "CityName");
            ViewBag.BusinessCategoryID = new SelectList(db.Business_Category, "BusinessCategoryID", "Business_Categoty");
            //return View(freelancer);
            return RedirectToAction("CreatePotfolio", new { id = (int)TempData["uID"] });

        }

        public ActionResult CreateEmployer()
        {

            ViewBag.SubhurbID = new SelectList(db.Subhurbs, "SubhurbID", "SubhurbName");
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Userpassword");
            ViewBag.CityID = new SelectList(db.Cities, "CityID", "CityName");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateEmployer([Bind(Include = " EmployerID, UserID,Name,Surname,BusinessName,SubhurbName,CityID")] typeEmployer employer)
        {
            if (ModelState.IsValid)
            {
                Subhurb sub = new Subhurb();
                sub.SubhurbID = 0;
                sub.SubhurbName = employer.SubhurbName;
                sub.CityID = employer.CityID;
                db.Subhurbs.Add(sub);
                db.SaveChanges();
                //model.orderline.Product = db.Products.Where(zz => zz.ProductID == model.orderline.ProductID).FirstOrDefault();
                //sub = new Subhurb();
                //sub = db.Subhurbs.Where(zz => zz.SubhurbName == employer.SubhurbName).FirstOrDefault();
                Employer emp = new Employer();
                emp.EmployerID = employer.EmployerID;
                emp.SubhurbID = sub.SubhurbID;
                emp.Name = employer.Name;
                emp.Surname = employer.Surname;
                emp.BusinessName = employer.BusinessName;
                emp.UserID = (int)TempData["uID"];
                db.Employers.Add(emp);
                //db.Users.Add(employer);
                db.SaveChanges();
                return RedirectToAction("Index", "Employer", new { id = (int)TempData["uID"] });
            }
            ViewBag.SubhurbID = new SelectList(db.Subhurbs, "SubhurbID", "SubhurbName");
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Userpassword");
            ViewBag.CityID = new SelectList(db.Cities, "CityID", "CityName");
            return RedirectToAction("Index", "Employer", new { id = (int)TempData["uID"] });
        }
        public ActionResult CreatePotfolio(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Freelancer freelancer = db.Freelancers.Where(zz => zz.UserID == id).FirstOrDefault();
            if (freelancer == null)
            {
                return HttpNotFound();
            }

            return View(freelancer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePotfolio(Freelancer freelancer, HttpPostedFileBase[] files)
        {

            int freelancerid = (int)TempData["uID"];
            var StudentData = db.Freelancers.Where(x => x.UserID == freelancerid).FirstOrDefault();

            if (ModelState.IsValid)
            {
                StudentData.PortfolioConcept = freelancer.PortfolioConcept;
                db.Entry(StudentData).State = EntityState.Modified;
                db.SaveChanges();

                foreach (HttpPostedFileBase file in files)
                {
                    if (file != null)
                    {
                        var inputImage = Path.GetFileName(file.FileName);
                        var imagePath = Path.Combine(Server.MapPath("~/Images") + inputImage);
                        file.SaveAs(imagePath);
                        int fID = StudentData.FreelancerID;
                        Image img = new Image();
                        img.ImgSource = "~/Images/" + file.FileName;
                        db.Images.Add(img);
                        db.SaveChanges();
                        int Id = img.ImageID;
                        PortfolioImage pf = new PortfolioImage();
                        pf.PortfolioID = 0;
                        pf.ImageID = Id;
                        pf.FreelancerID = StudentData.FreelancerID;
                        db.PortfolioImages.Add(pf);
                        db.SaveChanges();
                    }
                }
                return RedirectToAction("freeProfile", "Freel", new { id = (int)TempData["uID"] });
            }

            return RedirectToAction("freeProfile", "Freel", new { id = (int)TempData["uID"] });
        }
    }
}