﻿using INF272_del3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace INF272_del3.Controllers
{
    public class LoginController : Controller
    {
        private LinkDatabaseTDOEntities4 db = new LinkDatabaseTDOEntities4();
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(User objUser)
        {
            
                var obj = db.Users.Where(a => a.UserName.Equals(objUser.UserName) && a.Userpassword.Equals(objUser.Userpassword)).FirstOrDefault();
                if (obj != null)
                {
                    Session["UserID"] = obj.UserID.ToString();
                    Session["UserName"] = obj.UserName.ToString();
                    TempData["uID"] = obj.UserID;
                if (obj.AccountTypeID == 1)
                {
                    return RedirectToAction("Index", "Employer", new { id = (int)TempData["uID"] });
                }
                else if (obj.AccountTypeID == 2)
                {
                    return RedirectToAction("freeProfile", "Freel", new { id = (int)TempData["uID"] });
                }
                else if (obj.AccountTypeID == null && obj.Userpassword == "Admin" && obj.UserName == "Admin" && obj.UserStatus ==true)
                {
                    return RedirectToAction("Index", "Users");
                }

                }
               

            ViewBag.error = "Incorrect Details";
            return RedirectToAction("Index", "Login");
        }

    }
}