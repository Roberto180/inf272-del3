﻿using INF272_del3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data.Entity;
using System.Net;


namespace INF272_del3.Controllers
{
    public class AdminBusController : Controller
    {
        private LinkDatabaseTDOEntities4 db = new LinkDatabaseTDOEntities4();
        // GET: AdminBus
        public ActionResult Index()
        {
            var business_Category = db.Business_Category.Include(b => b.Image).Include(z=>z.Freelancers);
            
            return View(business_Category.ToList());
           
        }

        // GET: Business_Category/Details/5
      

        // GET: Business_Category/Create
        public ActionResult Create()
        {
            ViewBag.ImageID = new SelectList(db.Images, "ImageID", "ImgSource");
            return View();
        }

        // POST: Business_Category/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BusinessCategoryID,Business_Categoty,ImageID")] Business_Category business_Category, HttpPostedFileBase file)
        {
            string path = Path.Combine(Server.MapPath("~/Images"), Path.GetFileName(file.FileName));
            file.SaveAs(path);

            Image img = new Image();
            img.ImgSource = "~/Images/" + file.FileName;
            db.Images.Add(img);
            db.SaveChanges();
            int imageID = img.ImageID;

            if (ModelState.IsValid)
            {
                business_Category.ImageID = imageID;
                db.Business_Category.Add(business_Category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ImageID = new SelectList(db.Images, "ImageID", "ImgSource", business_Category.ImageID);
            return View(business_Category);
        }

        // GET: Business_Category/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Business_Category business_Category = db.Business_Category.Find(id);
            if (business_Category == null)
            {
                return HttpNotFound();
            }
            ViewBag.ImageID = new SelectList(db.Images, "ImageID", "ImgSource", business_Category.ImageID);
            return View(business_Category);
        }

        // POST: Business_Category/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( Business_Category business_Category)
        {

            int bid = business_Category.BusinessCategoryID;
            var category = db.Business_Category.Where(x => x.BusinessCategoryID == bid).FirstOrDefault();
            if (ModelState.IsValid)
            {
                category.Business_Categoty = business_Category.Business_Categoty;
                db.Entry(category).State = EntityState.Modified;
                db.SaveChanges();
            }
            
            return RedirectToAction("Index"); ;
        }

        // GET: Business_Category/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Business_Category business_Category = db.Business_Category.Find(id);
            if (business_Category == null)
            {
                return HttpNotFound();
            }
            return View(business_Category);
        }

        // POST: Business_Category/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Business_Category business_Category = db.Business_Category.Find(id);
            db.Business_Category.Remove(business_Category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
    
