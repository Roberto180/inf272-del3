﻿using INF272_del3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace INF272_del3.Controllers
{
    public class BookingController : Controller
    {
        private LinkDatabaseTDOEntities4 db = new LinkDatabaseTDOEntities4();
        public ActionResult Index(int ide, int id,  string message)
        {
            var idxx = db.Employers.Where(zz => zz.EmployerID == ide).Select(zz => zz.UserID).FirstOrDefault();
            EMP_Freelancer book = new EMP_Freelancer();
            book.EmployerID = ide;
            book.FreelancerID = id;
            
            book.Date = DateTime.Now;
            book.BookingRequest = message;
            db.EMP_Freelancer.Add(book);
            db.SaveChanges();
            return RedirectToAction("Index","Employer", new { id = idxx });
        }

      
    }
}