﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace INF272_del3.Viewmodels
{
    public class typeFreelancer
    {
        public int FreelancerID { get; set; }
        public Nullable<int> UserID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public Nullable<int> BusinessCategoryID { get; set; }
        public string Business_Category { get; set; }
        public string SubhurbName { get; set; }
        public Nullable<int> CityID { get; set; }
    }
}