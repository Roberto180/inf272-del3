﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace INF272_del3.Viewmodels
{
    public class typeEmployer
    {
        public int EmployerID { get; set; }
        public Nullable<int> UserID { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string BusinessName { get; set; }

        public string SubhurbName { get; set; }

        public Nullable<int> CityID { get; set; }
    }
}